(function( $ ) {

    $(document).ready(function(){
        $('.team__slider').slick({
            infinite: true,
            dots: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            prevArrow: $(".slider_nav .prev"),
            nextArrow: $(".slider_nav .next"),
            responsive: [
                {
                    breakpoint: 850,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        $("#up-page").on("click", function() {
            $("html, body").animate({
                scrollTop: 0
            }, 1000);
        });

        $(".header__arrows").on("click", function() {
            $("html, body").animate({
                scrollTop: $("#main_nav").offset().top
            }, 1000);
        });

        $(".nav__mobile-btn").on("click", function(){
            $(".nav-menu").toggleClass("show_menu");
            $(this).toggleClass("open");
        });

        $(".portfolio__btn").on("click", function(){
            $(".portfolio__nav").toggleClass("show_menu");
            $(this).toggleClass("open");
        });

        $(".main-nav").on("click", "a", function(){
           var href = $(this).attr("href");

            $("html, body").animate({
                scrollTop: $(href).offset().top
            }, 1000);
        });

        $('#contactForm').validate({
            // Rules for fields' validation
            rules:{
                "name":{
                    required:true,
                },
                "email":{required:true, email:true},
                "message":{required:true}
            },
            messages: {
                "name": "required",
                "email": "required",
                "message": "required"
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });

})( jQuery );